import random, heapq

def rollDice(numDice, numSides):
    rolls = []
    for i in range(numDice):
        rolls.append(random.randint(1, numSides))
    return rolls

def rollXDiceTakeHighestY(numDice, numSides, highestY):
    rolls = rollDice(numDice, numSides)
    return heapq.nlargest(highestY, rolls)

def rollXDiceTakeLowestY(numDice, numSides, lowestY):
    rolls = rollDice(numDice, numSides)
    return heapq.nsmallest(lowestY, rolls)